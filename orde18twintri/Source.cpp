﻿#include "coord_rande.hpp"
#include "rectangular_equilateral_triangle_info.hpp"
int main()
{
	using namespace sprout::literals;
	constexpr std::pair<int, int> p = { 1, 3 };
	constexpr yumetodo::coord_range<int> a = p;
	constexpr yumetodo::coord_range<int> b = { 2, 3 };
	static_assert(2 == a.contain(b, yumetodo::is_normalized), "err");
	static_assert(2 == b.contain(a, yumetodo::is_normalized), "err");
	static_assert(a != b, "err");
	//constexpr auto aa = yumetodo::make_rectangular_equilateral_triangle_info_from_str<int>(u8"4,6R2"_sv);
}
