﻿#ifndef ORDE17TWINTRI_COORD_RANGE_HPP_
#define ORDE17TWINTRI_COORD_RANGE_HPP_
#include <utility>
#include <type_traits>
#include <algorithm>
namespace yumetodo {
	struct normalize_base_tag {};
	struct is_normalized_tag : normalize_base_tag {};
	struct is_not_normalized_tag : normalize_base_tag {};
	static constexpr is_normalized_tag is_normalized = {};
	static constexpr is_not_normalized_tag is_not_normalized = {};
	template<typename T>
	struct coord_range {
		static_assert(std::is_arithmetic<T>::value, "require arithmetic");
		using value_type = T;

		//public member variable
		value_type first;
		value_type second;

		//special ctor/function : DO NOT SPECIFY constpexr MANYALLY
#if 1
		coord_range() = default;
		coord_range(const coord_range&) = default;
		coord_range(coord_range&&) = default;
		coord_range& operator=(const coord_range&) = default;
		coord_range& operator=(coord_range&&) = default;
#endif

		//other ctor
#if 1
		constexpr coord_range(const value_type& n1, const value_type& n2) : first(n1), second(n2) {}
		template<
			typename T1, typename T2,
			typename std::enable_if<std::is_constructible<value_type, T1&&>::value && std::is_constructible<value_type, T2&&>::value, std::nullptr_t>::type = nullptr
		>
		constexpr coord_range(T1&& n1, T2&& n2) : first(std::forward<T1>(n1)), second(std::forward<T2>(n2)) {}
		template<
			typename T1,
			typename std::enable_if<std::is_constructible<value_type, const T1&>::value, std::nullptr_t>::type = nullptr
		>
		constexpr coord_range(const coord_range<T1>& c) : coord_range(c.first, c.second) {}
		template<
			typename T1, typename T2,
			typename std::enable_if<std::is_constructible<value_type, const T1&>::value && std::is_constructible<value_type, const T2&>::value, std::nullptr_t>::type = nullptr
		>
		constexpr coord_range(const std::pair<T1, T2>& p) : coord_range(p.first, p.second) {}
		template<
			typename T1,
			typename std::enable_if<std::is_constructible<value_type, T1&&>::value, std::nullptr_t>::type = nullptr
		>
		constexpr coord_range(coord_range<T1>&& c) : coord_range(std::forward<T1>(c.first), std::forward<T1>(c.second)) {}
		template<
			typename T1, typename T2,
			typename std::enable_if<std::is_constructible<value_type, T1&&>::value && std::is_constructible<value_type, T2&&>::value, std::nullptr_t>::type = nullptr
		>
		constexpr coord_range(std::pair<T1, T2>&& p) : coord_range(std::forward<T1>(p.first), std::forward<T2>(p.second)) {}
#endif

		//other assign operator
#if 1
		template<
			typename T1,
			typename std::enable_if<std::is_assignable<value_type, const T1&>::value, std::nullptr_t>::type = nullptr
		>
		coord_range& operator=(const coord_range<T1>& c)
		{
			this->first = c.first;
			this->second = c.second;
			return *this;
		}
		template<
			typename T1, typename T2,
			typename std::enable_if<std::is_assignable<value_type, const T1&>::value && std::is_assignable<value_type, const T2&>::value, std::nullptr_t>::type = nullptr
		>
		coord_range& operator=(const std::pair<T1, T2>& p)
		{
			this->first = p.first;
			this->second = p.second;
			return *this;
		}
		template<
			typename T1,
			typename std::enable_if<std::is_assignable<value_type, T1&&>::value, std::nullptr_t>::type = nullptr
		>
		coord_range& operator=(coord_range<T1>&& c)
		{
			this->first = std::forward<T1>(c.first);
			this->second = std::forward<T1>(c.second);
			return *this;
		}
		template<
			typename T1, typename T2,
			typename std::enable_if<std::is_assignable<value_type, T1&&>::value && std::is_assignable<value_type, T2&&>::value, std::nullptr_t>::type = nullptr
		>
		coord_range& operator=(std::pair<T1, T2>&& c)
		{
			this->first = std::forward<T1>(c.first);
			this->second = std::forward<T2>(c.second);
			return *this;
		}
#endif

		//member function
		constexpr bool operator==(const coord_range& o) const noexcept
		{
			return this->first == o.first && this->second == o.second;
		}
		constexpr bool operator!=(const coord_range& o) const noexcept { return !(*this == o); }
		constexpr bool contain(value_type n) const noexcept
		{
			return (this->first < this->second) ? (this->first <= n && n <= this->second) : (this->second <= n && n <= this->first);
		}
		constexpr coord_range<value_type> normalize() const noexcept { return std::minmax(this->first, this->second); }
		constexpr std::size_t count(is_normalized_tag) const noexcept { return this->second - this->first + 1; }
		constexpr std::size_t count(is_not_normalized_tag = {}) const noexcept { return this->normalize().cout(is_normalized); }
		constexpr std::size_t contain(const coord_range<value_type>& o, is_normalized_tag) const noexcept
		{
			return (this->second < o.first || o.second < this->first) ? 0
				: coord_range<value_type>{ std::max(this->first, o.first), std::min(this->second, o.second) }.count(is_normalized);
		}
		constexpr std::size_t contain(const coord_range<value_type>& o, is_not_normalized_tag = {}) const noexcept
		{
			return this->normalize().contain(o.normalize(), is_normalized);
		}
	};
	namespace {
		template<typename T, typename std::enable_if<std::is_arithmetic<T>::value, std::nullptr_t>::type = nullptr>
		constexpr coord_range<T> make_coord_range(T n1, T n2) { return std::minmax(n1, n2); }
		template<typename T, typename std::enable_if<std::is_arithmetic<T>::value, std::nullptr_t>::type = nullptr>
		constexpr coord_range<T> make_coord_range(T n) { return { n, n }; }
	}
}
#endif //ORDE17TWINTRI_COORD_RANGE_HPP_
