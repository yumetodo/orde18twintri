﻿#ifndef ORDE17TWINTRI_RECTANGULAR_EQUILATERAL_TRIANGLE_INFO_HPP_
#define ORDE17TWINTRI_RECTANGULAR_EQUILATERAL_TRIANGLE_INFO_HPP_
#include <cstddef>
#include <cstdint>
#include <sprout/utility/string_view.hpp>
#include <sprout/string.hpp>

namespace yumetodo {
	enum class rectangular_equilateral_triangle_direction : std::uint8_t {
		left = 0,
		top = 1,
		right = 2,
		bottom = 3
	};
	template<typename T>
	struct rectangular_equilateral_triangle_info {
		using coord_type = T;
		coord_type ax;
		coord_type ay;
		std::size_t h;
		rectangular_equilateral_triangle_direction direction;
		constexpr coord_type left_x() const
		{
			return (rectangular_equilateral_triangle_direction::left == this->direction) ? this->ax : coord_type(this->ax - this->h + 1);
		}
		constexpr coord_type top_y() const
		{
			return (rectangular_equilateral_triangle_direction::top == this->direction) ? this->ay : coord_type(this->ay - this->h + 1);
		}
		constexpr coord_type right_x() const
		{
			return (rectangular_equilateral_triangle_direction::right == this->direction) ? this->ax : coord_type(this->ax - this->h + 1);
		}
		constexpr coord_type bottom_y() const
		{
			return (rectangular_equilateral_triangle_direction::bottom == this->direction) ? this->ay : coord_type(this->ay - this->h + 1);
		}
	};
	template<typename T>
	using reti = rectangular_equilateral_triangle_info<T>;
	template<typename T1, typename T2>
	constexpr bool has_possibility_overlap(const rectangular_equilateral_triangle_info<T2>& t1, const rectangular_equilateral_triangle_info<T2>& t2)
	{
		return (t1.left_x() <= t2.right_x() || t2.left_x() <= t1.right_x()) && (t1.top_y() <= t2.bottom_y() || t2.top_y() <= t1.bottom_y());
	}
	namespace make_rectangular_equilateral_triangle_info_from_str_impl {
		template<typename T, typename CharType>
		constexpr rectangular_equilateral_triangle_info<T> parse_height(
			T ax, T ay, rectangular_equilateral_triangle_direction d, const sprout::basic_string_view<CharType>& str
		)
		{
			return { ax, ay, sprout::from_string<std::size_t>(str), d };
		}
		template<typename CharType> constexpr sprout::basic_string<CharType, 5> direction_strs();
		template<> constexpr sprout::basic_string<char, 5> direction_strs<char>() { return u8"LTRB"; }
		template<> constexpr sprout::basic_string<wchar_t, 5> direction_strs<wchar_t>() { return L"LTRB"; }
#ifdef SPROUT_USE_UNICODE_LITERALS
		template<> constexpr sprout::basic_string<char16_t, 5> direction_strs<char16_t>() { return u"LTRB"; }
		template<> constexpr sprout::basic_string<char32_t, 5> direction_strs<char32_t>() { return U"LTRB"; }
#endif
		template<typename CharType> constexpr sprout::basic_string<CharType, 5> direction_strs_v = direction_strs<CharType>();

		template<typename T, typename CharType>
		constexpr rectangular_equilateral_triangle_info<T> parse_direction(T ax, T ay, const sprout::basic_string_view<CharType>& str)
		{
			return parse_height(
				ax, ay,
				(direction_strs_v<CharType>[0] == str.front()) ? rectangular_equilateral_triangle_direction::left
				: (direction_strs_v<CharType>[1] == str.front()) ? rectangular_equilateral_triangle_direction::top
				: (direction_strs_v<CharType>[2] == str.front()) ? rectangular_equilateral_triangle_direction::right
				: (direction_strs_v<CharType>[3] == str.front()) ? rectangular_equilateral_triangle_direction::bottom
				: throw std::invalid_argument("Fail to find direction charactor"),
				str.substr(1)
			);
		}
		template<typename T, typename CharType>
		constexpr rectangular_equilateral_triangle_info<T> parse_y(T ax, const sprout::basic_string_view<CharType>& str)
		{
			return parse_direction(ax, sprout::from_string<T>(str), str.substr(str.find_first_of(direction_strs_v<CharType>)));
		}
		template<typename CharType> struct comma;
		template<> struct comma<char> : std::integral_constant<char, u8","[0]> {};
		template<> struct comma<wchar_t> : std::integral_constant<wchar_t, L','> {};
#ifdef SPROUT_USE_UNICODE_LITERALS
		template<> struct comma<char16_t> : std::integral_constant<char16_t, u','> {};
		template<> struct comma<char32_t> : std::integral_constant<char32_t, U','> {};
#endif
		template<typename CharType> constexpr CharType comma_v = comma<CharType>::value;
		template<typename T, typename CharType>
		constexpr rectangular_equilateral_triangle_info<T> parse_x(const sprout::basic_string_view<CharType>& str)
		{
			return parse_y(sprout::from_string<T>(str), str.substr(str.find_first_of(comma_v<CharType>) + 1));
		}
	}
	template<typename T, typename CharType>
	constexpr rectangular_equilateral_triangle_info<T> make_rectangular_equilateral_triangle_info_from_str(const sprout::basic_string_view<CharType>& str)
	{
		return make_rectangular_equilateral_triangle_info_from_str_impl::parse_x<T>(str);
	}

}
#endif //ORDE17TWINTRI_RECTANGULAR_EQUILATERAL_TRIANGLE_INFO_HPP_
